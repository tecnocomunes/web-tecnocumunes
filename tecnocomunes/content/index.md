+++
title = "Web Tecnocomunes"
author = ["david"]
date = 2021-02-27T00:00:00-03:00
tags = ["index"]
draft = false
+++

## Proyectos {#proyectos}

Esta web está destinada a mostrar los proyectos ligados a la idea del
**tecnocomún**, tanto propios como iniciativas que tienen el mismo sentido.

Entre los proyectos principales llevados a cabo y creados bajo esta idea están:

-   Curso de programación para organizaciones comunitarias
-   Plataforma de educación
-   Ciencia comunitaria
-   Libro de programación en Gnu/Linux, uso de la terminal
-   Escuela de ciencias y tecnologías


## Teoría {#teoría}

En esta sección se recolectan documentos y artículos relativos al desarrollo de las ideas compartidas en este lugar.


## Servicios {#servicios}

Los servicios disponibles que tenemos actualmente habilitados son

Taiga
: para la gestión de proyectos.

Penpot
: para el diseño de prototipos de interfaces.

Correo electrónico
: para quienes participen o tengan iniciativas en el **procomún**.


## Blogs {#blogs}

Los blogs son espacios dedicados a diferentes temas, desarrollados de manera
personal por quienes los escriban.

Entre los que se encuentran disponible son

Programación e Informática
: considera software libre, lenguajes de
    programación, algoritmos y diferentes técnicas de desarrollo.


Cultura
: comprende comentarios sobre libros, películas, series y
    videojuegos con una opinión personal.


## Contacto {#contacto}

El contacto principal es mediante el correo electrónico a **admin@tecnocomunes.org**.
